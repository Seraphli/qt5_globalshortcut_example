#-------------------------------------------------
#
# Project created by QtCreator 2013-10-04T19:58:08
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GlobalShortcut
TEMPLATE = app


SOURCES += main.cpp \
    mainwindow.cpp

HEADERS  += mainwindow.h \
    winEventFilter.h

FORMS    += mainwindow.ui
