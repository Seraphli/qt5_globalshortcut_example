#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    RegisterHotKey((HWND)this->winId(), MOD_CONTROL ^ VK_F8, MOD_CONTROL, VK_F8);
}

MainWindow::~MainWindow()
{
    delete ui;
    UnregisterHotKey((HWND)this->winId(), MOD_CONTROL ^ VK_F8);
}
