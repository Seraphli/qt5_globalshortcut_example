#include <qt_windows.h>
#include <QAbstractNativeEventFilter>

class winEventFilter : public QAbstractNativeEventFilter
{
public:
    virtual bool nativeEventFilter(const QByteArray &eventType, void *message, long *) Q_DECL_OVERRIDE
    {

        if (eventType == "windows_dispatcher_MSG" || eventType == "windows_generic_MSG")
        {
            MSG *ev = static_cast<MSG *>(message);
            if(ev->message == WM_HOTKEY)
            {
                if(ev->lParam == ((VK_F8<<16)+MOD_CONTROL))
                    qDebug("i came here");
                qDebug("wParam:%ld",(quint32)ev->wParam);
                qDebug("lParam:%ld",(quint32)ev->lParam);
            }
            // ...
        }
        return false;
    }
};
